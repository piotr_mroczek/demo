<?php
namespace Mroczek\Homepage\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class IndexController
{
    public function IndexAction(Request $request, Application $app)
    {
        return $app->redirect(
            $app['url_generator']->generate('users_list')
        );
    }
}
