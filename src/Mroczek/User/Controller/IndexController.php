<?php
namespace Mroczek\User\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Mroczek\User\Entity\UserRepository  as Repository;
use Mroczek\User\Entity\UserEntity      as Entity;

class IndexController
{
    public function ListAction(Request $request, Application $app)
    {
        $dbh            = new \PDO('mysql:host=localhost;dbname=regita', 'regita','regita');
        $dbh->query("SET NAMES utf8");
        $entityManager  = new Repository($dbh);
        $users          = $entityManager->fetchAll();

        return $app['twig']->render(
            'User/Index/list.twig',
             ['users' => $users]
        );
    }

    public function CreateAction(Request $request, Application $app)
    {
        $form = $app['form.factory']->createBuilder('form', null, array('csrf_protection' => false))
            ->add('name')
            ->add('surname')
            ->add('pesel')
            ->add('documentNo')
            ->add('status')
            ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->bind($request);

            if ($form->isValid()) {

                $data = $form->getData();

                $v = new Entity;

                $v->setName(
                    $data['name']
                );

                $v->setSurname(
                    $data['surname']
                );

                $v->setPesel(
                    $data['pesel']
                );

                $v->setDocumentNo(
                    $data['documentNo']
                );

                $v->setStatus(
                    $data['status']
                );

                $dbh            = new \PDO('mysql:host=localhost;dbname=regita', 'regita','regita');
                $dbh->query("SET NAMES utf8");
                $entityManager  = new Repository($dbh);
                $entityManager->save($v);

                return $app->redirect(
                    $app['url_generator']->generate('users_list')
                );
            }
        }

        return $app['twig']->render(
            'User/Index/create.twig',
            ['form' => $form->createView()]
        );
    }

    public function ReadAction(Request $request, Application $app)
    {
        $id = $request->get('id');
        $id = intval($id);

        if (!$id) {
            $app->abort(404, "User $id does not exist.");
        }

        $dbh            = new \PDO('mysql:host=localhost;dbname=regita', 'regita','regita');
        $dbh->query("SET NAMES utf8");
        $entityManager  = new Repository($dbh);

        try {
            $user = $entityManager->find($id);
        }
        catch (\InvalidArgumentException $e) {

            $app->abort(404, "User $id does not exist.");
        }

        return $app['twig']->render(
            'User/Index/read.twig',
            ['user' => $user]
        );
    }

    public function UpdateAction(Request $request, Application $app)
    {
        $id = $request->get('id');
        $id = intval($id);

        if (!$id) {
            $app->abort(404, "User $id does not exist.");
        }

        return $app['twig']->render(
            'User/Index/update.twig'
        );
    }

    public function DeleteAction(Request $request, Application $app)
    {
        $id = $request->get('id');
        $id = intval($id);

        if (!$id) {
            $app->abort(404, "User $id does not exist.");
        }

        $dbh = new \PDO('mysql:host=localhost;dbname=regita', 'regita','regita');
        $dbh->query("SET NAMES utf8");
        $entityManager  = new Repository($dbh);

        $entityManager->delete($id);

        return $app->redirect(
            $app['url_generator']->generate('users_list')
        );
    }
}
