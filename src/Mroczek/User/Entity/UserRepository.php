<?php

namespace Mroczek\User\Entity;

use Mroczek\User\Entity\UserEntity as Entity;

class UserRepository {

    private $tableName = 'klienci';
    private $pk        = 'id_klient';

    /** @var  \PDO */
    private $dbh;

    function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;
    }

    protected function mapEntity(array $data) {

        $dataTransformer = function (array $data) {

            $v = new Entity;

            $v->setId(
                $data['id_klient']
            );

            $v->setName(
                $data['imie']
            );

            $v->setSurname(
                $data['nazwisko']
            );

            $v->setPesel(
                $data['pesel']
            );

            $v->setDocumentNo(
                $data['nr_dokumentu']
            );

            $v->setStatus(
                $data['status']
            );

            return $v;
        };

        return call_user_func($dataTransformer, $data);
    }

    protected function mapEntitySet (array $data) {

        $that = $this;
        $entitySetTransformer = function ($data) use ($that) {

            return $this->mapEntity($data);
        };

        return array_map($entitySetTransformer, $data);
    }

    public function fetchAll() {

        $sql = sprintf("SELECT * FROM %s ;", $this->tableName);
        $sth = $this->dbh->prepare($sql);
        $sth->execute();

        return $this->mapEntitySet(
            $sth->fetchAll()
        );
    }

    public function find($id) {

        $sql = sprintf("SELECT * FROM %s WHERE %s = %d;",
            $this->tableName,
            $this->pk,
            $id
        );

        $sth = $this->dbh->prepare($sql);
        $sth->execute();

        if (false === $data = $sth->fetch()) {

            $msg = sprintf('Object with this %s = %d does not exists.',
                $this->pk,
                $id
            );

            throw new \InvalidArgumentException($msg);
        }

        return $this->mapEntity(
            $data
        );
    }


    public function delete($id) {

        $sql = sprintf("DELETE FROM %s WHERE %s = %d;",
            $this->tableName,
            $this->pk,
            $id
        );

        $sth = $this->dbh->prepare($sql);
        $sth->execute();
    }

    public function save(Entity $v) {

        // TODO: implementacja kiedy zapisujemy encej z obencym ID czyli robimy UPDATE
        $sql = "INSERT INTO `%s` ( `imie` , `nazwisko` , `pesel` , `nr_dokumentu` , `status`)
                VALUES ('%s',  '%s',  %d,  %d,  %d);";
        $sql = sprintf(
            $sql,
            $this->tableName,
            $v->getName(),
            $v->getSurname(),
            $v->getPesel(),
            $v->getDocumentNo(),
            $v->getStatus()
        );

        try {
            $this->dbh->exec($sql);

        }   catch (\PDOException $e)
        {
            echo 'Wystapił blad biblioteki PDO: ' . $e->getMessage();exit;
        }
    }

}