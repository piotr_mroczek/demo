<?php

// web/index.php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();


$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));
$app->before(function () use ($app) {
    $app['twig']->addGlobal('layout', $app['twig']->loadTemplate('layout.twig'));
});
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

use Silex\Provider\FormServiceProvider;
$app->register(new FormServiceProvider());









$app->get('/', 'Mroczek\Homepage\Controller\IndexController::IndexAction')
    ->bind('homepage')
;

$app->get('/uzytkownicy', 'Mroczek\User\Controller\IndexController::ListAction')
    ->bind('users_list')
;

$app->get('/uzytkownicy/dodaj', 'Mroczek\User\Controller\IndexController::CreateAction')
    ->method('GET|POST')
    ->bind('user_create');

$app->get('/uzytkownicy/{id}/edytuj', 'Mroczek\User\Controller\IndexController::UpdateAction')
    ->bind('user_update')
;

$app->get('/uzytkownicy/{id}/pokaz', 'Mroczek\User\Controller\IndexController::ReadAction')
    ->bind('user_read')
;

$app->get('/uzytkownicy/{id}/usun', 'Mroczek\User\Controller\IndexController::DeleteAction')
    ->bind('user_delete')
;



$app['debug'] = true;
$app->run();